function countLetter(letter, sentence) {
    if(letter.length!==1){
        return undefined;
    }
    let result = 0;

    for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
            result++;
        }
    }
    return result
}

function isIsogram(text) {
    text = text.toLowerCase()

    for(let i =0;i<text.length;i++){
        let result=0;

        for(let j=0;j<text.length;j++){
            if(text[i]===text[j]){
                result++;
                if(result>1){
                    return false;
                }
            }
        }
    }
    return true;
}

function purchase(age, price) {
    if(age<13){
        return undefined;
    }else if(age >=13 && age <= 21){
        price *= 0.8;
    }else if(age >= 65){
        price *= 0.8;
    }
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    price = Math.round(price*100)/100
    return `${price}`
}

function findHotCategories(items) {

const products=[
    {id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries'},
    {id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries'},
    {id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries'},
    {id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets'},
    {id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets'}
    ]
let categories=[];
    for(let product of products){
        if(product.stocks===0 && !categories.includes(product.category)){
            categories.push(product.category);
        }
    }

    return categories
}

function findFlyingVoters(candidateA, candidateB) {
    let commonVoters = []
    for(let voter of candidateA){
        if(candidateB.includes(voter)){
            commonVoters.push(voter)
        }
    }
    return commonVoters
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};